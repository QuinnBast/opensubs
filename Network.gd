extends Node

# Config variables

var Config := ConfigFile.new()

# Connection variables

var UserId : int

# Inbuilt functions

func _ready() -> void:
	randomize()
	if Config.load(get_config_path()) != OK:
		Config.set_value("Connection", "UserId", randi())
		Config.save(get_config_path())
		get_tree().change_scene("res://Scenes/Menus/ConnectionMenu.tscn")
	else:
		Config.load(get_config_path())
		UserId = Config.get_value("Connection", "UserId")
		connect_to_server()

# Config functions

# Returns the location of the config file
func get_config_path() -> String:
	return OS.get_executable_path().get_base_dir().plus_file("Client.cfg")

# Connection functions

func connect_to_server() -> void:
	print("Attempting to connect to server")
	var HostIp = Config.get_value("Connection", "HostIp")
	var HostPort = Config.get_value("Connection", "HostPort")
	var Network := NetworkedMultiplayerENet.new()
	Network.create_client(HostIp, HostPort)
	get_tree().set_network_peer(Network)
	Network.connect("connection_succeeded", self, "connection_succeeded")
	Network.connect("connection_failed", self, "connection_failed")

func connection_succeeded() -> void:
	print("Connection successful")
	get_tree().change_scene("res://Scenes/Menus/MainMenu.tscn")

func connection_failed() -> void:
	print("Connection failed")

remote func username_approved() -> void:
	print("Username approved")

remote func username_rejected() -> void:
	print("Username rejected")

remote func set_game_setup_options(maxplayers : int, ticklengths : Array) -> void:
	get_node("/root/MainMenu").set_game_setup_options(maxplayers, ticklengths)

remote func set_global_variable(variable : String, value) -> void:
	if GlobalVariables.get(variable) != null:
		GlobalVariables.set(variable, value)
		match variable:
			"GlobalTick":
				print("Updating global tick to " + str(value))
	else:
		# Failure case
		print("Invalid global variable")

remote func set_player(player : String, value : Dictionary) -> void:
	GlobalVariables.Players[player] = value
	if get_tree().current_scene.name == "Game":
		get_node("/root/Game").set_game_start_info()

remote func game_state_updated() -> void:
	print("Fully updated game state")
	print("Switching to game")
	get_tree().change_scene("res://Scenes/Base/Game.tscn")

remote func set_outpost_data(value : Dictionary) -> void:
	print("Receiving outpost data")
	if value.has("OutpostName"):
		for i in len(GlobalVariables.OutpostData):
			if GlobalVariables.OutpostData[i]["OutpostName"] == value["OutpostName"]:
				GlobalVariables.OutpostData.remove(i)
				break
		GlobalVariables.OutpostData.append(value)
		if get_tree().current_scene.name == "Game":
			get_node("/root/Game").set_outpost_data(value)
	else:
		# Failure case
		print("History does not contain outpost name")

remote func set_submarine_data(value : Dictionary) -> void:
	print("Receiving submarine data")
	if value.has("SubmarineName"):
		for i in len(GlobalVariables.SubmarineData):
			if GlobalVariables.SubmarineData[i]["SubmarineName"] == value["SubmarineName"]:
				GlobalVariables.SubmarineData.remove(i)
				break
		GlobalVariables.SubmarineData.append(value)
		if get_tree().current_scene.name == "Game":
			get_node("/root/Game").set_submarine_data(value)
	else:
		# Failure case
		print("Invalid submarine name")

remote func set_outpost_history(tick : int, outpostname : String, value : Array, overridetemporary : bool = true) -> void:
	print("Receiving outpost \"" + str(outpostname) + "\" history")
	for outpost in GlobalVariables.OutpostData:
		if outpost["OutpostName"] == outpostname:
			set_history_slice(tick, outpost["History"], value)
			if get_tree().current_scene.name == "Game":
				get_node("/root/Game").set_outpost_history(outpostname, outpost["History"], overridetemporary)
			return
	# Failure case
	print("Invalid outpost name")

remote func set_submarine_history(tick : int, submarinename : String, value : Array, overridetemporary : bool = true) -> void:
	print("Receiving submarine \"" + str(submarinename) + "\" history")
	for submarine in GlobalVariables.SubmarineData:
		if submarine["SubmarineName"] == submarinename:
			set_history_slice(tick, submarine["History"], value)
			if get_tree().current_scene.name == "Game":
				get_node("/root/Game").set_submarine_history(submarinename, submarine["History"])
			return
	# Failure case
	print("Invalid submarine name")

func set_history_slice(tick : int, history : Array, value : Array) -> void:
	for i in history[-1]["Tick"] - tick + 1:
		history.remove(len(history) - 1)
	history.append_array(value)

remote func update_visible_tick() -> void:
	if get_tree().current_scene.name == "Game":
		get_node("/root/Game").update_visible_tick()

remote func focus_submarine(submarinename : String) -> void:
	get_node("/root/Game").focus_submarine(submarinename)

remote func remove_submarine(submarinename : String) -> void:
	for i in len(GlobalVariables.SubmarineData):
		if GlobalVariables.SubmarineData[i]["SubmarineName"] == submarinename:
			GlobalVariables.SubmarineData.remove(i)
			break
	get_node("/root/Game").remove_submarine(submarinename)

remote func set_joined_games(games : Dictionary) -> void:
	get_node("/root/MainMenu").set_joined_games(games)

remote func set_open_games(games : Dictionary) -> void:
	get_node("/root/MainMenu").set_open_games(games)

remote func add_joined_game(gameid : int, gameinfo : Dictionary) -> void:
	print("Joining game")
	get_node("/root/MainMenu").remove_open_game(gameid)
	get_node("/root/MainMenu").add_joined_game(gameid, gameinfo)

remote func add_online_player(player : String, id : int) -> void:
	if GlobalVariables.OnlinePlayers.get(player) == null:
		GlobalVariables.OnlinePlayers[player] = {"Ids" : []}
	GlobalVariables.OnlinePlayers[player]["Ids"].append(id)
	get_node("/root/Game").update_online_players()

remote func remove_online_player(player : String, id : int) -> void:
	GlobalVariables.OnlinePlayers[player]["Ids"].erase(id)
	if len(GlobalVariables.OnlinePlayers[player]["Ids"]) == 0:
		GlobalVariables.OnlinePlayers.erase(player)
	get_node("/root/Game").update_online_players()
