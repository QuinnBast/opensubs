extends Control

onready var JoinedGameButton = preload("res://Scenes/InterfaceElements/JoinedGameButton.tscn")
onready var OpenGameButton = preload("res://Scenes/InterfaceElements/OpenGameButton.tscn")

func _ready() -> void:
	Network.rpc_id(1, "get_joined_games", get_tree().get_network_unique_id(), GlobalVariables.Username)
	Network.rpc_id(1, "get_open_games", get_tree().get_network_unique_id(), GlobalVariables.Username)
	Network.rpc_id(1, "get_game_setup_options", get_tree().get_network_unique_id())

func set_game_setup_options(maxplayers : int, ticklengths : Array) -> void:
	$Menus/CreateGame/PlayersSpinBox.max_value = maxplayers
	for length in ticklengths:
		$Menus/CreateGame/TickLengthOptionButton.add_item(str(length)) 

func switch_button(button : Node) -> void:
	for child in $MenuButtons.get_children():
		child.pressed = button == child

func switch_menu(menu : Node) -> void:
	for child in $Menus.get_children():
		child.visible = menu == child

func set_joined_games(gamesinfo : Dictionary) -> void:
	for child in $Menus/OpenGames/ScrollContainer/VBoxContainer.get_children():
		child.queue_free()
	for gameid in gamesinfo.keys():
		var JoinedGameButtonInstance = JoinedGameButton.instance()
		JoinedGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$Menus/JoinedGames/ScrollContainer/VBoxContainer.add_child(JoinedGameButtonInstance)

func set_open_games(gamesinfo : Dictionary) -> void:
	for child in $Menus/OpenGames/ScrollContainer/VBoxContainer.get_children():
		child.queue_free()
	for gameid in gamesinfo.keys():
		var OpenGameButtonInstance = OpenGameButton.instance()
		OpenGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$Menus/OpenGames/ScrollContainer/VBoxContainer.add_child(OpenGameButtonInstance)

func add_joined_game(gameid : int, gameinfo : Dictionary) -> void:
	var JoinedGameButtonInstance = JoinedGameButton.instance()
	JoinedGameButtonInstance.set_game(gameid, gameinfo)
	$Menus/JoinedGames/ScrollContainer/VBoxContainer.add_child(JoinedGameButtonInstance)

func remove_open_game(gameid : int) -> void:
	for child in $Menus/OpenGames/ScrollContainer/VBoxContainer.get_children():
		if child.GameId == gameid:
			child.queue_free()
			break

func _on_JoinedGamesButton_pressed() -> void:
	switch_button($MenuButtons/JoinedGamesButton)
	switch_menu($Menus/JoinedGames)

func _on_OpenGamesButton_pressed() -> void:
	switch_button($MenuButtons/OpenGamesButton)
	switch_menu($Menus/OpenGames)

func _on_CreateGameButton_pressed() -> void:
	switch_button($MenuButtons/CreateGameButton)
	switch_menu($Menus/CreateGame)

func _on_CreateButton_pressed() -> void:
	if $Menus/CreateGame/GameNameEdit.text != "":
		Network.rpc_id(
			1,
			"create_game", 
			get_tree().get_network_unique_id(),
			$Menus/CreateGame/GameNameEdit.text,
			$Menus/CreateGame/PlayersSpinBox.value,
			int($Menus/CreateGame/TickLengthOptionButton.get_item_text($Menus/CreateGame/TickLengthOptionButton.selected)),
			GlobalVariables.Username
		)
		switch_button($MenuButtons/JoinedGamesButton)
		switch_menu($Menus/JoinedGames)
		$Menus/CreateGame/GameNameEdit.text = ""
		$Menus/CreateGame/PlayersSpinBox.value = $Menus/CreateGame/PlayersSpinBox.min_value
		$Menus/CreateGame/TickLengthOptionButton.selected = 0
