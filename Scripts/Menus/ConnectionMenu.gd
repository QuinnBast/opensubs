extends Control

func _on_JoinButton_pressed() -> void:
	var HostIp : String = $HostIpLineEdit.text
	var HostPort : int = int($HostPortLineEdit.text)
	var Username : String = $UsernameLineEdit.text
	if HostIp != "" and HostPort != 0 and Username != "":
		GlobalVariables.Username = Username
		Network.Config.set_value("Connection", "HostIp", HostIp)
		Network.Config.set_value("Connection", "HostPort", HostPort)
		Network.connect_to_server()

func disable_input() -> void:
	$UsernameLineEdit.editable = false
	$HostIpLineEdit.editable = false
	$HostPortLineEdit.editable = false
	$JoinButton.disabled = true
