extends Node

# Config variables

var TickLength : int
var SubmarineSpeed : int
var SubmarineWaitModifier : int
var OutpostStartingTroops : int
var GenerateTroopTicks : int
var GenerateShieldTicks : int
var GenerateTroopNumber : int
var GenerateShieldNumber : int
var VisibleTickMax : int
var CalculateTroopsDelay : int
var Colors : Dictionary

# Game variables

var GameId : int
var OnlinePlayers : Dictionary
var GlobalTick : int = 0
var Paused : bool
var Players : Dictionary
var Username : String
var OutpostData : Array
var SubmarineData : Array
var CursorPosition : Vector2
var HoveredStartOutpost : Node
var HoveredEndOutpost : Node
var VisibleTick : int = 0
var IsJumping : bool = false
