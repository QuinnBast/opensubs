extends Node2D

onready var Generator : PackedScene = preload("res://Scenes/Outposts/Generator.tscn")
onready var Factory : PackedScene = preload("res://Scenes/Outposts/Factory.tscn")
onready var Submarine : PackedScene = preload("res://Scenes/Submarines/Submarine.tscn")
onready var PreparingLine : PackedScene = preload("res://Scenes/Lines/PreparingLine.tscn")
onready var SubmarineLine : PackedScene = preload("res://Scenes/Lines/SubmarineLine.tscn")
onready var OnlineIndicator : PackedScene = preload("res://Scenes/InterfaceElements/OnlineIndicator.tscn")
onready var Panels : Dictionary = {
	"Preparing" : $Interface/Panels/PreparingPanel,
	"Launching" : $Interface/Panels/LaunchingPanel,
	"Time" : $Interface/Panels/TimePanel,
	"Submarine" : $Interface/Panels/SubmarinePanel,
	"Factory" : $Interface/Panels/FactoryPanel,
	"Generator" : $Interface/Panels/GeneratorPanel,
	"Exit" : $Interface/Panels/ExitPanel
}

export var DistanceToHoverOutpost : float = 50.0
export var MapCursorMovementAllowance : float = 10.0

var OldMapCursorPosition : Vector2
var OldCameraPosition : Vector2
var MapCursorMovementMax : float = 0.0
var ZoomMultiplier : float = 2.0

# Inbuilt functions

func _ready() -> void:
	set_tick(GlobalVariables.GlobalTick)
	set_game_start_info()
	update_online_players()
	for outpost in GlobalVariables.OutpostData:
		set_outpost_data(outpost)
	for submarine in GlobalVariables.SubmarineData:
		set_submarine_data(submarine)

func _process(delta : float) -> void:
	if Input.is_action_just_released("ZoomIn") or Input.is_action_just_released("ZoomOut"):
		var ZoomChange : float = 0
		if Input.is_action_just_released("ZoomIn"):
			ZoomChange = -(ZoomMultiplier * delta)
		elif Input.is_action_just_released("ZoomOut"):
			ZoomChange = ZoomMultiplier * delta
		$Camera/Camera2D.zoom = Vector2(clamp($Camera/Camera2D.zoom.x + ZoomChange, 0.1, 10), clamp($Camera/Camera2D.zoom.y + ZoomChange, 0.1, 10))
		$Camera/MapButton.rect_scale = Vector2(clamp($Camera/MapButton.rect_scale.x + ZoomChange, 0.1, 10), clamp($Camera/MapButton.rect_scale.y + ZoomChange, 0.1, 10))
	if $Camera/MapButton.pressed:
		$Camera.global_position += OldMapCursorPosition - get_global_mouse_position()
		var MapCursorMovementDifference : float = OldMapCursorPosition.distance_to(get_global_mouse_position())
		if MapCursorMovementDifference > MapCursorMovementMax:
			MapCursorMovementMax = MapCursorMovementDifference
	GlobalVariables.HoveredEndOutpost = null
	for child in $Outposts.get_children():
		if child != GlobalVariables.HoveredStartOutpost:
			if get_global_mouse_position().distance_to(child.InitialPosition) <= DistanceToHoverOutpost:
				GlobalVariables.HoveredEndOutpost = child
				child.focus()
				GlobalVariables.CursorPosition = child.InitialPosition
			elif child.Focused:
				child.unfocus()
	if GlobalVariables.HoveredEndOutpost == null:
		GlobalVariables.CursorPosition = get_global_mouse_position()
	if Input.is_action_just_released("Click"):
		if GlobalVariables.VisibleTick == GlobalVariables.GlobalTick:
			if GlobalVariables.HoveredStartOutpost != null and GlobalVariables.HoveredEndOutpost != null:
				var SelectedOutpostHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, GlobalVariables.HoveredStartOutpost.History)
				if SelectedOutpostHistoryEntry["CurrentPlayer"] == GlobalFunctions.get_username_color(GlobalVariables.Username):
					create_submarine()
		$Lines/RulerLine.hide()
		$Lines/RulerLabel.hide()
		GlobalVariables.HoveredStartOutpost = null

# Tick functions

func set_tick(tick : int) -> void:
	GlobalVariables.VisibleTick = tick
	for child in $Submarines.get_children():
		child.update_tick(false)
	for child in $Outposts.get_children():
		child.update_tick(false)
	for child in $Interface/Panels.get_children():
		if child.has_method("update_tick"):
			if child.has_method("set_temporary_troops"):
				child.update_tick(false)
			else:
				child.update_tick()

func update_visible_tick() -> void:
	if GlobalVariables.VisibleTick + 1 == GlobalVariables.GlobalTick and GlobalVariables.IsJumping == false:
		set_tick(GlobalVariables.GlobalTick)

func jump_to_tick(tick : int) -> void:
	GlobalVariables.IsJumping = true
	$JumpTween.interpolate_property(GlobalVariables, "VisibleTick", GlobalVariables.VisibleTick, min(tick, GlobalVariables.VisibleTickMax), 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$JumpTween.start()
	show_panel_string("Time")

# Data functions

func set_outpost_data(value : Dictionary) -> void:
	for child in $Outposts.get_children():
		if child.OutpostName == value["OutpostName"]:
			for variable in value:
				if child.get(variable) != null:
					child.set(variable, value[variable])
				else:
					# Failure case
					print("Invalid outpost variable")
			return
	var OutpostInstance : Node
	var TickHistory : int = GlobalFunctions.get_tick_history(GlobalVariables.VisibleTick, value["History"])
	if value["History"][TickHistory]["CurrentType"] == "Factory":
		OutpostInstance = Factory.instance()
	elif value["History"][TickHistory]["CurrentType"] == "Generator":
		OutpostInstance = Generator.instance()
	else:
		# Failure case
		print("Invalid outpost type")
		return
	for variable in value:
		if OutpostInstance.get(variable) != null:
			OutpostInstance.set(variable, value[variable])
		else:
			# Failure case
			print("Invalid outpost variable")
	$Outposts.add_child(OutpostInstance)

func set_submarine_data(value : Dictionary) -> void:
	for child in $Submarines.get_children():
		if child.SubmarineName == value["SubmarineName"]:
			for variable in value:
				if child.get(variable) != null:
					child.set(variable, value[variable])
				else:
					# Failure case
					print("Invalid submarine variable")
			return
	var SubmarineInstance : Node = Submarine.instance()
	for variable in value:
		if SubmarineInstance.get(variable) != null:
			SubmarineInstance.set(variable, value[variable])
		else:
			# Failure case
			print("Invalid submarine variable")
	$Submarines.add_child(SubmarineInstance)
	SubmarineInstance.prepare()

func set_outpost_history(outpostname : String, value : Array, overridetemporary : bool = true) -> void:
	for child in $Outposts.get_children():
		if child.OutpostName == outpostname:
			child.History = value
			child.update_tick(overridetemporary)
			return
	# Failure case
	print("Invalid outpost name")

func set_submarine_history(submarinename : String, value : Array, overridetemporary : bool = true) -> void:
	for child in $Submarines.get_children():
		if child.SubmarineName == submarinename:
			child.History = value
			child.update_tick(overridetemporary)
			return
	# Failure case
	print("Invalid submarine name")

func create_submarine() -> void:
	Network.rpc_id(1, "create_submarine", GlobalVariables.GameId, get_tree().get_network_unique_id(), GlobalVariables.VisibleTick, GlobalVariables.HoveredStartOutpost.OutpostName, GlobalVariables.Username, GlobalVariables.HoveredEndOutpost.OutpostName)

func remove_submarine(submarinename : String) -> void:
	for child in $Submarines.get_children():
		if child.SubmarineName == submarinename:
			child.remove()
			break

func focus_submarine(submarinename : String) -> void:
	unfocus_outposts()
	unfocus_submarines()
	var SubmarineNode : Node = get_source_node(submarinename)
	$Interface/Panels/LaunchingPanel.Submarine = SubmarineNode
	$Interface/Panels/PreparingPanel.Submarine = SubmarineNode
	$Interface/Panels/SubmarinePanel.Submarine = SubmarineNode
	$Interface/Panels/PreparingPanel.Outpost = get_source_node(SubmarineNode.InitialOutpost)
	SubmarineNode.Selected = true
	SubmarineNode.focus()

# Node functions

func get_source_node(source : String) -> Node:
	for child in $Outposts.get_children():
		if child.OutpostName == source:
			return child
	for child in $Submarines.get_children():
		if child.SubmarineName == source:
			return child
	# Failure case
	print("Invalid source name")
	return Node.new()

func create_prepare_line() -> Node:
	var PreparingLineInstance : Node = PreparingLine.instance()
	$Lines.add_child(PreparingLineInstance)
	return PreparingLineInstance

func create_submarine_line() -> Node:
	var SubmarineLineInstance : Node = SubmarineLine.instance()
	$Lines.add_child(SubmarineLineInstance)
	return SubmarineLineInstance

# Visual functions

func set_game_start_info() -> void:
	var OccupiedPlayers : Array = []
	for player in GlobalVariables.Players:
		if GlobalVariables.Players[player]["Username"] != "Empty":
			if GlobalVariables.Players[player]["Occupied"] == true:
				OccupiedPlayers.append(player)
	if len(OccupiedPlayers) < len(GlobalVariables.Players) -1:
		$Interface/GameStartLabel.text = "Waiting for more players to join (" + str(len(OccupiedPlayers)) + "/" + str(len(GlobalVariables.Players) - 1) + " present)"
	else:
		$Interface/GameStartLabel.hide()

func select_outpost(outpostname : String) -> void:
	unfocus_outposts()
	unfocus_submarines()
	var Outpost : Node = get_source_node(outpostname)
	var OutpostHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)
	Outpost.Selected = true
	$Interface/Panels/FactoryPanel.Outpost = Outpost
	$Interface/Panels/GeneratorPanel.Outpost = Outpost
	switch_outpost_panel(OutpostHistoryEntry["CurrentType"])

func unfocus_outposts() -> void:
	hide_panel_string("Generator")
	hide_panel_string("Factory")
	for child in $Outposts.get_children():
		child.Selected = false
	$Lines/RulerLine.hide()
	if GlobalVariables.HoveredEndOutpost != null:
		GlobalVariables.HoveredEndOutpost.unfocus()
		GlobalVariables.HoveredEndOutpost = null

func unfocus_submarines() -> void:
	hide_panel_string("Submarine")
	hide_panel_string("Preparing")
	hide_panel_string("Launching")
	for child in $Submarines.get_children():
		child.unfocus()
		child.Selected = false

func show_panel(panel : Node) -> void:
	if panel.visible == false:
		panel.show()
		if panel.has_method("prepare"):
			panel.prepare()
		if panel == Panels["Time"]:
			$Interface/Buttons/ClockButton.pressed = true
		elif panel == Panels["Exit"]:
			$Interface/Buttons/ExitButton.pressed = true

func hide_panel(panel : Node) -> void:
	if panel.visible:
		panel.hide()
		if panel == Panels["Time"]:
			$Interface/Buttons/ClockButton.pressed = false
		elif panel == Panels["Exit"]:
			$Interface/Buttons/ExitButton.pressed = false

func hide_panel_string(panelstring : String) -> void:
	hide_panel(Panels[panelstring])

func show_panel_string(panelstring : String) -> void:
	show_panel(Panels[panelstring])

func hide_all_panels() -> void:
	for child in $Interface/Panels.get_children():
		hide_panel(child)

func switch_outpost_panel(panelstring : String) -> void:
	var OutpostPanels : Array = ["Generator", "Factory"]
	for panel in OutpostPanels:
		if panel == panelstring:
			show_panel_string(panelstring)
		else:
			hide_panel_string(panelstring)

func update_online_players() -> void:
	for child in $Interface/OnlineIndicators.get_children():
		child.queue_free()
	var OnlinePlayers = GlobalVariables.OnlinePlayers.duplicate(true)
	for player in OnlinePlayers:
		if OnlinePlayers[player]["Ids"].has(get_tree().get_network_unique_id()):
			OnlinePlayers.erase(player)
	var Keys = OnlinePlayers.keys()
	$Interface/OnlinePlayersLabel.visible = len(Keys) > 0
	for i in len(Keys):
		var OnlineIndicatorInstance = OnlineIndicator.instance()
		var XPosition = -20 + -30 * i
		OnlineIndicatorInstance.rect_position.x = XPosition
		OnlineIndicatorInstance.modulate = Keys[i]
		$Interface/OnlineIndicators.add_child(OnlineIndicatorInstance)
		$Interface/OnlinePlayersLabel.rect_global_position.x = OnlineIndicatorInstance.rect_global_position.x -100

# Signals

func _on_ClockButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		show_panel_string("Time")
	else:
		hide_panel_string("Time")
		set_tick(GlobalVariables.GlobalTick)

func _on_MapButton_button_down() -> void:
	OldMapCursorPosition = get_global_mouse_position()
	OldCameraPosition = $Camera/Camera2D.global_position

func _on_MapButton_button_up() -> void:
	if MapCursorMovementMax <= MapCursorMovementAllowance:
		hide_panel_string("Time")
		unfocus_outposts()
		unfocus_submarines()
	MapCursorMovementMax = 0
	OldMapCursorPosition = Vector2()

func _on_JumpTween_tween_step(object : Object, key : NodePath, elapsed : float, value : float) -> void:
	set_tick(int(value))

func _on_JumpTween_tween_completed(object : Object, key : NodePath) -> void:
	GlobalVariables.IsJumping = false

func _on_ExitButton_pressed() -> void:
	hide_all_panels()
	show_panel_string("Exit")

func _on_NoButton_pressed() -> void:
	hide_panel_string("Exit")

func _on_YesButton_pressed() -> void:
	Network.rpc_id(1, "remove_players_id", GlobalVariables.GameId, get_tree().get_network_unique_id())
	get_tree().change_scene("res://Scenes/Menus/MainMenu.tscn")

func _on_ExitButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		show_panel_string("Exit")
	else:
		hide_panel_string("Exit")
