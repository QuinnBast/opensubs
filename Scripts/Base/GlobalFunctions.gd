extends Node

# Returns the index in history for passed tick
func get_tick_history(tick : int, history : Array) -> int:
	for i in len(history):
		if history[i]["Tick"] == tick:
			return i
	# Failure case
	print("Invalid tick")
	return -1

# Returns the entry in history for passed tick
func get_tick_history_entry(tick : int, history : Array) -> Dictionary:
	var TickHistory = get_tick_history(tick, history)
	if TickHistory != -1:
		return history[TickHistory]
	else:
		# Failure case
		return {}

# Converts a number of seconds to a HH:MM:SS format
func get_time_text(time : int) -> String:
	var TimeText : String
	var Seconds : int = int(time) % 60
	TimeText = str(Seconds)
	if Seconds <= 9:
		TimeText = "0" + TimeText
	var Minutes : int = (int(time) / 60) % 60
	if Minutes > 0:
		TimeText = str(Minutes) + ":" + TimeText
		if Minutes <= 9:
			TimeText = "0" + TimeText
	else:
		TimeText = "00:" + TimeText
	var Hours : int = int(time) / 3600
	if Hours > 0:
		TimeText = str(Hours) + ":" + TimeText
	return TimeText

# Returns the username's color
func get_username_color(username : String) -> String:
	for player in GlobalVariables.Players:
		if GlobalVariables.Players[player]["Username"] == username:
			return player
	return "FFFFFF"
