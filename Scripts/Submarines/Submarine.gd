extends Node2D

onready var Game : Node = get_node("/root/Game")
onready var PreparingPanel : Node = get_node("/root/Game/Interface/Panels/PreparingPanel")
onready var LaunchingPanel : Node = get_node("/root/Game/Interface/Panels/LaunchingPanel")
onready var SubmarinePanel : Node = get_node("/root/Game/Interface/Panels/SubmarinePanel")

var SubmarineName : String
var History : Array = []
var InitialOutpost : String
var InitialPlayer : String
var PreparingLine : Node
var SubmarineLine : Node
var Selected : bool = false
var Cancelled : bool = false
var TemporaryTroops : bool = false

# Start functions

# Initial setup
func prepare() -> void:
	PreparingLine = Game.create_prepare_line()
	PreparingLine.Submarine = SubmarineName
	SubmarineLine = Game.create_submarine_line()
	SubmarineLine.Submarine = SubmarineName
	$LaunchTimer.start(GlobalVariables.TickLength * GlobalVariables.SubmarineWaitModifier)
	$LaunchTimer.paused = GlobalVariables.Paused
	update_tick()

# Launch submarine
func launch() -> void:
	if GlobalVariables.VisibleTick == GlobalVariables.GlobalTick:
		if PreparingPanel.Submarine == self:
			Game.hide_panel_string("Preparing")
			Game.hide_panel_string("Launching")
			PreparingPanel.update_submarine_troops()
		update_tick()

# Tick functions

# Updates the submarine to show it's state at the passed tick
func update_tick(overridetemporary : bool = true) -> void:
	$LaunchTimer.paused = GlobalVariables.Paused
	var HistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, History)
	if Cancelled == false and len(HistoryEntry) > 0:
		$Sprites/BodySprites.modulate = HistoryEntry["CurrentPlayer"]
		var TargetNode : Node = Game.get_source_node(HistoryEntry["Target"])
		set_panel()
		if $LaunchTimer.is_stopped() == false and HistoryEntry["Preparing"]:
			PreparingLine.set_line_points([HistoryEntry["Position"], TargetNode.get_game_position(GlobalVariables.VisibleTick)])
			switch_line(PreparingLine)
			if $PrepareMoveAnimation.is_playing() == false:
				$PrepareMoveAnimation.play("PrepareMove")
		else:
			SubmarineLine.set_line_points([HistoryEntry["Position"], TargetNode.get_game_position(GlobalVariables.VisibleTick)])
			switch_line(SubmarineLine)
			$PrepareMoveAnimation.stop()
			$Sprites.position = Vector2()
		show()
		position = HistoryEntry["Position"]
		if overridetemporary:
			TemporaryTroops = false
		if TemporaryTroops == false:
			$Sprites/TroopsLabel.text = str(HistoryEntry["TroopTotal"])
		set_course()
		update_gift_sprites()
	else:
		hide_submarine()

# Data functions

# Points submarine in direction of target
func set_course() -> void:
	var HistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, History)
	var DirectionAngle : float = HistoryEntry["Position"].angle_to_point(Game.get_source_node(HistoryEntry["Target"]).get_game_position(GlobalVariables.VisibleTick))
	rotation = DirectionAngle - PI
	var IsFlipped : bool = rad2deg(DirectionAngle) < 90 and rad2deg(DirectionAngle) > -90
	if IsFlipped:
		$Sprites.scale.y = -1
		$Sprites/TroopsLabel.rect_scale.x = -1
	else:
		$Sprites.scale.y = 1
		$Sprites/TroopsLabel.rect_scale.x = 1

# Sends remove submarine call
func cancel_prepare() -> void:
	Cancelled = true
	hide()
	PreparingLine.hide_line()
	SubmarineLine.hide_line()
	print("Sending submarine cancellation to server")
	Network.rpc_id(1, "remove_submarine", GlobalVariables.GameId, SubmarineName)

# Removes submarine and associated nodes
func remove() -> void:
	PreparingLine.queue_free()
	SubmarineLine.queue_free()
	queue_free()

# Gets all submarine data to send to server
func get_data() -> Dictionary:
	return {
		"SubmarineName" : SubmarineName,
		"InitialOutpost" : InitialOutpost,
		"History" : History
	}

# Gets the submarine's position at tick
func get_game_position(tick : int) -> Vector2:
	return GlobalFunctions.get_tick_history_entry(tick, History)["Position"]

# Jumps time controller to submarine arrival
func jump_to_arrival() -> void:
	Game.jump_to_tick(History[-1]["Tick"] + 1)

# Visual functions

# Sets the correct panels to be visible
func set_panel() -> void:
	if Selected:
		var HistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, History)
		if (
			GlobalVariables.VisibleTick == GlobalVariables.GlobalTick and
			$LaunchTimer.is_stopped() == false and
			HistoryEntry["Preparing"] and
			HistoryEntry["CurrentPlayer"] == GlobalFunctions.get_username_color(GlobalVariables.Username)
		):
			Game.show_panel_string("Preparing")
			Game.show_panel_string("Launching")
			Game.hide_panel_string("Submarine")
		else:
			Game.hide_panel_string("Preparing")
			Game.hide_panel_string("Launching")
			Game.show_panel_string("Submarine")

# Sets visual focused effects
func focus() -> void:
	set_panel()
	$Sprites/SelectionSprites.visible = true

# Sets visual unfocused effects
func unfocus() -> void:
	$Sprites/SelectionSprites.visible = false

# Hides the submarine
func hide_submarine() -> void:
	if Selected:
		Game.hide_panel_string("Submarine")
	SubmarineLine.hide_line()
	PreparingLine.hide_line()
	hide()

# Sets sprites to correspond with gift status
func update_gift_sprites() -> void:
	var IsGift : bool = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, History)["Gift"]
	$Sprites/BodySprites/SubmarineSprite.visible = not IsGift
	$Sprites/BodySprites/GiftSprite.visible = IsGift
	$Sprites/ShadowSprites/SubmarineShadowSprite.visible = not IsGift
	$Sprites/ShadowSprites/GiftShadowSprite.visible = IsGift
	$Sprites/SelectionSprites/SubmarineOutlineSprite.visible = not IsGift
	$Sprites/SelectionSprites/GiftOutlineSprite.visible = IsGift

# Sets the visibility of course lines
func switch_line(line : Node) -> void:
	if Cancelled == false:
		PreparingLine.switch_line(line)
		SubmarineLine.switch_line(line)

# Temporarily change troops whilst awaiting history from server
func set_temporary_troops(troops) -> void:
	TemporaryTroops = true
	$Sprites/TroopsLabel.text = str(troops)

# Signals

# Calls launch()
func _on_LaunchTimer_timeout() -> void:
	launch()

# Makes submarine panels visible
func _on_Button_pressed() -> void:
	Game.focus_submarine(SubmarineName)
