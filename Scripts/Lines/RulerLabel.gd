extends Label

func _process(delta : float) -> void:
	if GlobalVariables.HoveredStartOutpost != null:
		show()
		if GlobalVariables.HoveredEndOutpost == null:
			rect_position = GlobalVariables.CursorPosition - rect_pivot_offset - Vector2(0, 20)
		else:
			rect_position = GlobalVariables.CursorPosition - rect_pivot_offset - Vector2(0, 80)
		text = str(GlobalFunctions.get_time_text(ceil(GlobalVariables.HoveredStartOutpost.InitialPosition.distance_to(GlobalVariables.CursorPosition) / GlobalVariables.SubmarineSpeed * GlobalVariables.TickLength / 10) * 10))
