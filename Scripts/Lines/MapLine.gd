extends Line2D

onready var LineButton : PackedScene = preload("res://Scenes/Lines/LineButton.tscn")
onready var LineButtonInstance : Node

func _ready() -> void:
	LineButtonInstance = LineButton.instance()
	get_node("/root/Game/LineButtons").add_child(LineButtonInstance)
	LineButtonInstance.connect("pressed", self, "_on_Button_pressed")

func set_line_points(linepoints : Array) -> void:
	points = linepoints
	LineButtonInstance.rect_size.x = points[0].distance_to(points[1])
	LineButtonInstance.rect_global_position = points[0] - LineButtonInstance.rect_pivot_offset
	LineButtonInstance.rect_rotation = rad2deg(points[1].angle_to_point(points[0]))

func hide_line() -> void:
	hide()
	LineButtonInstance.hide()

func show_line() -> void:
	show()
	LineButtonInstance.show()

func switch_line(line : Node) -> void:
	if self == line:
		show_line()
	else:
		hide_line()
