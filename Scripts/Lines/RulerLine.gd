extends "res://Scripts/Lines/MapLine.gd"

func _process(delta : float) -> void:
	if GlobalVariables.HoveredStartOutpost != null:
		show()
		set_line_points([GlobalVariables.HoveredStartOutpost.InitialPosition, GlobalVariables.CursorPosition])
