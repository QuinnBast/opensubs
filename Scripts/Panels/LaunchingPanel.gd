extends Panel

var Troops : int = 0
var Submarine : Node

func _process(delta : float) -> void:
	if Submarine != null:
		$Info/ExtraLabel.text = "Drillers\nLaunch in " + GlobalFunctions.get_time_text(Submarine.get_node("LaunchTimer").time_left)

func prepare() -> void:
	var SubmarineHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)
	set_troops(SubmarineHistoryEntry["TroopTotal"])
	$Header.modulate = GlobalFunctions.get_username_color(GlobalVariables.Username)
	$PlayerLabel.text = GlobalVariables.Username
	$GiftButton.pressed = SubmarineHistoryEntry["Gift"]

func set_troops(troops : int) -> void:
	$Info/TroopsLabel.text = str(troops)
	$Info/TroopsLabel.hide()
	$Info/TroopsLabel.show()
	$Info/ExtraLabel.rect_position.x = $Info/TroopsLabel.rect_position.x + $Info/TroopsLabel.rect_size.x + 5

func set_temporary_troops(troops : int) -> void:
	$Info/TroopsLabel.text = str(troops)

func cancel_prepare() -> void:
	var Game = get_node("/root/Game")
	Game.hide_panel(Game.panels("Launching"))
	Submarine = null

func _on_GiftButton_toggled(buttonpressed : bool) -> void:
	if GlobalVariables.VisibleTick == GlobalVariables.GlobalTick:
		Network.rpc_id(1, "gift_submarine", GlobalVariables.GameId, GlobalVariables.VisibleTick, Submarine.SubmarineName, buttonpressed)
	else:
		$GiftButton.pressed = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)["Gift"]

func _on_ClockButton_pressed() -> void:
	Submarine.jump_to_arrival()
