extends Panel

onready var LaunchingPanel : Node = get_node("/root/Game/Interface/Panels/LaunchingPanel")
onready var SubmarinePanel : Node = get_node("/root/Game/Interface/Panels/SubmarinePanel")

var Outpost : Node
var Submarine : Node
var TemporaryTroops : bool = false

func prepare() -> void:
	$OutpostLabel.text = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)["CurrentType"].to_upper()
	update_tick()
	$TroopSelect.value = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)["TroopTotal"]

func update_tick(overridetemporary : bool = false) -> void:
	$Header.modulate = GlobalFunctions.get_username_color(GlobalVariables.Username)
	if visible:
		$TroopSelect.max_value = get_available_troops()
		if overridetemporary:
			TemporaryTroops = false
		if TemporaryTroops == false:
			$TroopsLabel.text = str(GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)["TroopTotal"])

func update_submarine_troops() -> void:
	Network.rpc_id(1, "prepare_submarine_troops", GlobalVariables.GameId, Submarine.SubmarineName, $TroopSelect.value)

func set_temporary_troops(troops : int) -> void:
	TemporaryTroops = true
	$TroopsLabel.text = str(troops)

func set_temporary_outpost_troops(troops : int) -> void:
	Outpost.set_temporary_troops(troops)
	set_temporary_troops(troops)

func set_temporary_submarine_troops(troops : int) -> void:
	Submarine.set_temporary_troops(troops)
	LaunchingPanel.set_temporary_troops(troops)

func cancel_prepare() -> void:
	$CalculateTimer.stop()
	get_node("/root/Game").hide_panel_string("Preparing")
	LaunchingPanel.cancel_prepare()
	SubmarinePanel.cancel_prepare()
	Outpost.set_temporary_troops(get_available_troops())
	Submarine.cancel_prepare()
	Submarine = null

func get_available_troops() -> int:
	var OutpostHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)
	var SubmarineHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)
	return OutpostHistoryEntry["TroopTotal"] + SubmarineHistoryEntry["TroopTotal"]

func _on_TroopSelect_value_changed(value : int) -> void:
	if visible:
		set_temporary_outpost_troops($TroopSelect.max_value - value)
		set_temporary_submarine_troops(value)
		if GlobalVariables.CalculateTroopsDelay == -1:
			update_submarine_troops()
		else:
			$CalculateTimer.start(GlobalVariables.CalculateTroopsDelay)

func _on_CancelButton_pressed() -> void:
	cancel_prepare()

func _on_CalculateTimer_timeout() -> void:
	update_submarine_troops()
