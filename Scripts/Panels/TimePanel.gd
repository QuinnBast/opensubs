extends Control

export var AdjustMultiplier : float = 0.08
export var WheelChangeThreshold : int = 2
export var ArrowChangeThreshold : int = 10

var Value : float = 0.0
var OldValue : float = 0.0
var WheelDifference : float = 0.0
var OldAngle : int = 0

func _process(delta : float) -> void:
	update_time_label()
	if $Wheel/WheelButton.pressed and GlobalVariables.IsJumping == false:
		$Wheel/WheelPoint.look_at(get_global_mouse_position())
		var NewWheelDifference : float = clamp(stepify($Wheel/WheelPoint.rotation_degrees - OldAngle, 1 / AdjustMultiplier), -Value, (GlobalVariables.VisibleTickMax / AdjustMultiplier) - Value - AdjustMultiplier)
		if abs(NewWheelDifference) >= WheelChangeThreshold:
			WheelDifference = NewWheelDifference
	var AdjustValue = Value + WheelDifference
	if AdjustValue != OldValue:
		get_node("/root/Game").set_tick(int(AdjustValue * AdjustMultiplier))
		$Wheel/WheelSprite.rotation_degrees = AdjustValue
		OldValue = AdjustValue

func prepare() -> void:
	Value = GlobalVariables.VisibleTick / AdjustMultiplier

func update_tick() -> void:
	if GlobalVariables.IsJumping:
		Value = GlobalVariables.VisibleTick / AdjustMultiplier

func update_time_label() -> void:
	var Time : int = ((Value + WheelDifference) * AdjustMultiplier - GlobalVariables.GlobalTick) * GlobalVariables.TickLength
	if Time >= 0:
		$TimeLabel.text = GlobalFunctions.get_time_text(Time)  + " from now"
	else:
		$TimeLabel.text = GlobalFunctions.get_time_text(-Time)  + " ago"

func _on_WheelButton_button_down() -> void:
	$Wheel/WheelPoint.look_at(get_global_mouse_position())
	OldAngle = $Wheel/WheelPoint.rotation_degrees

func _on_WheelButton_button_up() -> void:
	if WheelDifference == 0:
		var WheelCursorMovement : float = get_global_mouse_position().x - $Wheel/WheelPoint.global_position.x
		if abs(WheelCursorMovement) > ArrowChangeThreshold:
			var NewArrowDifference = (WheelCursorMovement / abs(WheelCursorMovement)) / AdjustMultiplier
			Value = clamp(Value + NewArrowDifference, 0, GlobalVariables.VisibleTickMax / AdjustMultiplier - AdjustMultiplier)
	else:
		Value += WheelDifference
		WheelDifference = 0
