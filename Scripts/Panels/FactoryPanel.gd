extends "res://Scripts/Panels/OutpostPanel.gd"

func get_production_info() -> String:
	var OutpostHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)
	return (
		"\n+" + str(OutpostHistoryEntry["TroopNumberMultiplierTotal"] * GlobalVariables.GenerateTroopNumber) +
		" in " + GlobalFunctions.get_time_text(get_ticks_to_production() * GlobalVariables.TickLength) +
		" and every " + GlobalFunctions.get_time_text(OutpostHistoryEntry["TroopTimeMultiplierTotal"] * GlobalVariables.GenerateTroopTicks * GlobalVariables.TickLength)
		)

func get_ticks_to_production() -> int:
	var TicksToProduction = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)["TroopTimeCountdown"]
	if TicksToProduction == 0:
		TicksToProduction = 20
	return TicksToProduction

func _on_ClockButton_pressed() -> void:
	get_node("/root/Game").jump_to_tick(GlobalVariables.VisibleTick + get_ticks_to_production() )
