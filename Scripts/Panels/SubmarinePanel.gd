extends Panel

var Submarine : Node
var TimeToArrival : String

func prepare() -> void:
	update_tick()
	var SubmarineHistoryEntry = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)
	$GiftButton.visible = SubmarineHistoryEntry["CurrentPlayer"] == GlobalFunctions.get_username_color(GlobalVariables.Username)

func update_troops() -> void:
	$Info/TroopsLabel.text = str(GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)["TroopTotal"])

func update_tick() -> void:
	if visible:
		var SubmarineHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)
		update_troops()
		var Player = SubmarineHistoryEntry["CurrentPlayer"]
		$Header.modulate = Player
		$PlayerLabel.text = GlobalVariables.Players[Player]["Username"]
		$GiftButton.pressed = SubmarineHistoryEntry["Gift"]
		if SubmarineHistoryEntry["Gift"]:
			$SubmarineLabel.text = "SUB (GIFT)"
		else:
			$SubmarineLabel.text = "SUB"
		var TicksToArrival : int = (Submarine.History[-1]["Tick"] + 1) - GlobalVariables.VisibleTick
		TimeToArrival = GlobalFunctions.get_time_text(TicksToArrival * GlobalVariables.TickLength)
		$Info/ExtraLabel.text = "Drillers\nArrives in " + str(TimeToArrival) + "\nSpeed multiplier " + str(SubmarineHistoryEntry["SpeedMultiplierTotal"])

func cancel_prepare() -> void:
	Submarine = null

func _on_GiftButton_toggled(buttonpressed : bool) -> void:
	if GlobalVariables.VisibleTick == GlobalVariables.GlobalTick:
		Network.rpc_id(1, "gift_submarine", GlobalVariables.GameId, GlobalVariables.VisibleTick, Submarine.SubmarineName, buttonpressed)
	else:
		$GiftButton.pressed = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Submarine.History)["Gift"]

func _on_ClockButton_pressed() -> void:
	Submarine.jump_to_arrival()
