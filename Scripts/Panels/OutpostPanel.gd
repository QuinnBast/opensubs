extends Panel

var Outpost : Node

func _ready() -> void:
	hide()

func prepare() -> void:
	update_tick()

func update_tick() -> void:
	if Outpost != null:
		var OutpostHistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)
		var Player = OutpostHistoryEntry["CurrentPlayer"]
		$Header.modulate = Player
		$OutpostLabel.text = OutpostHistoryEntry["CurrentType"].to_upper()
		$PlayerLabel.text = GlobalVariables.Players[Player]["Username"]
		$TroopsLabel.text = str(GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, Outpost.History)["TroopTotal"])
		$TroopsLabel.hide()
		$TroopsLabel.show()
		$InfoLabel.text = "Drillers" + get_production_info() + get_electrical_info()
		$InfoLabel.rect_position.x = $TroopsLabel.rect_position.x + $TroopsLabel.rect_size.x + 5

func get_production_info() -> String:
	return ""

func get_electrical_info() -> String:
	return ""
