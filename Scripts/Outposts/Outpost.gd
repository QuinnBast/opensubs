extends Control

onready var Game := get_node("/root/Game")

export var SelectedSizeIncrease : int = 1.1
export var InitialType : String
export var OutpostName : String
export var InitialPlayer : String
export var ShieldCirclePoints : int = 50
export var InitialShieldMax : int = 40
export var OutpostCursorMovementAllowance : float = 10.0

var Selected : bool = false
var Focused : bool = false
var InitialPosition : Vector2
var History : Array = []
var OldOutpostCursorPosition : Vector2
var OutpostCursorMovementMax : float = 0.0
var SubmarineCount : int = 0
var TemporaryTroops : bool = false

# Inbuilt functions

func _ready() -> void:
	$BlockMoveAnimation.play("BlockMove")
	$Labels/NameLabel.text = OutpostName
	rect_global_position = InitialPosition - rect_pivot_offset
	update_tick()

func _process(delta : float) -> void:
	if GlobalVariables.HoveredStartOutpost == self:
		var OutpostCursorMovementDifference : float = OldOutpostCursorPosition.distance_to(get_global_mouse_position())
		if OutpostCursorMovementDifference > OutpostCursorMovementMax:
			OutpostCursorMovementMax = OutpostCursorMovementDifference

# Tick functions

func update_tick(overridetemporary : bool = true) -> void:
	var HistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, History)
	$Sprites/OutpostSprite.modulate = HistoryEntry["CurrentPlayer"]
	if overridetemporary:
		TemporaryTroops = false
	if TemporaryTroops == false:
		$Labels/TroopsLabel.text = str(HistoryEntry["TroopTotal"])
	update_shield_rings()
	if Selected:
		Game.switch_outpost_panel(HistoryEntry["CurrentType"])

# Data functions

func get_game_position(tick : int) -> Vector2:
	return InitialPosition

# Visual functions

func focus() -> void:
	if Focused == false:
		$FocusAnimation.play("Focus")
	Focused = true

func unfocus() -> void:
	if Focused:
		$FocusAnimation.play("Unfocus")
	Focused = false

# Temporarily change troops whilst awaiting history from server
func set_temporary_troops(troops) -> void:
	TemporaryTroops = true
	$Labels/TroopsLabel.text = str(troops)

func update_shield_rings() -> void:
	var HistoryEntry : Dictionary = GlobalFunctions.get_tick_history_entry(GlobalVariables.VisibleTick, History)
	var ShieldFraction : float = float(HistoryEntry["ShieldTotal"]) / float(HistoryEntry["ShieldMax"])
	var UsedShieldCirclePoints : int = ceil(ShieldCirclePoints * ShieldFraction)
	var ShieldRings : float = ceil(HistoryEntry["ShieldMax"] / 10)
	for child in $Shield/ShieldLines.get_children():
		child.hide()
	for i in ShieldRings:
		var ShieldLines : Node = $Shield/ShieldLines.get_child(i)
		ShieldLines.show()
		var UnusedShieldCoords : Array = []
		for secondaryi in ShieldCirclePoints + 1:
			UnusedShieldCoords.append(Vector2(70 + 10 * i, 0).rotated(deg2rad(40 + (280.0 / ShieldCirclePoints) * secondaryi)))
		var UsedShieldCoords : Array = []
		for secondaryi in UsedShieldCirclePoints:
			UsedShieldCoords.append(Vector2(70 + 10 * i, 0).rotated(deg2rad(40 + (280.0 / ShieldCirclePoints) * secondaryi)))
		UsedShieldCoords.append(Vector2(70 + 10 * i, 0).rotated(deg2rad(40 + 280.0 * ShieldFraction)))
		if i == ShieldRings - 1:
			UsedShieldCoords.append(Vector2(90 + 10 * i, 0).rotated(deg2rad(40 + 280.0  * ShieldFraction)))
		ShieldLines.get_node("UnusedShieldLine").points = UnusedShieldCoords
		ShieldLines.get_node("UsedShieldLine").points = UsedShieldCoords
	$Shield/ShieldLabel.text = str(HistoryEntry["ShieldTotal"])
	$Shield/ShieldLabel.rect_position = Vector2(100 + 10 * ShieldRings, 0).rotated(deg2rad(40 + (280.0 / ShieldCirclePoints) * ShieldCirclePoints * ShieldFraction)) - $Shield/ShieldLabel.rect_size / 2

# Signals

func _on_Button_button_down() -> void:
	GlobalVariables.HoveredStartOutpost = self
	if GlobalVariables.HoveredEndOutpost == self:
		GlobalVariables.HoveredEndOutpost = null
	OldOutpostCursorPosition = get_global_mouse_position()

func _on_Button_button_up() -> void:
	if OutpostCursorMovementMax <= OutpostCursorMovementAllowance:
		Game.select_outpost(OutpostName)
	OutpostCursorMovementMax = 0
	OldOutpostCursorPosition = Vector2()
